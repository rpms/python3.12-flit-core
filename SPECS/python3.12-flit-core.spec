%global __python3 /usr/bin/python3.12
%global python3_pkgversion 3.12

# Workaround for https://issues.redhat.com/browse/CS-1907
# By defining the %%{__bootstrap} value,
# we prevent misconfigured Koji putting a literal %%{__bootstrap} in %%dist.
%{!?__bootstrap:%global __bootstrap %{nil}}

# When bootstrapping new Python we need to build flit in bootstrap mode.
# The Python RPM dependency generators and pip are not yet available.
%bcond_with bootstrap

# RHEL: disable tests due to missing dependencies
%bcond_with tests

Name:           python%{python3_pkgversion}-flit-core
Version:        3.9.0
Release:        3%{?dist}
Summary:        PEP 517 build backend for packages using Flit

# flit-core is BSD (BSD-3-Clause)
# flit_core/versionno.py contains a regex that is from packaging, BSD (BSD-2-Clause)
License:        BSD

URL:            https://flit.pypa.io/
Source:         %{pypi_source flit_core}

BuildArch:      noarch
BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-rpm-macros
%if %{without bootstrap}
BuildRequires:  python%{python3_pkgversion}-pip
%endif

%if %{with tests}
BuildRequires:  python3-pytest
# Test deps that require flit-core to build:
BuildRequires:  python3-testpath
%endif

%global _description %{expand:
This provides a PEP 517 build backend for packages using Flit.
The only public interface is the API specified by PEP 517,
at flit_core.buildapi.}

%description %_description

# RPM generators are not yet available when we bootstrap in Fedora,
# in RHEL we bootstrap the same way
%if %{with bootstrap}
Provides:       python%{python3_pkgversion}dist(flit-core) = %{version}
Provides:       python%{python3_pkgversion}dist(flit-core) = %{version}
Requires:       python(abi) = %{python3_pkgversion}
%endif


%prep
%autosetup -p1 -n flit_core-%{version}

# Remove vendored tomli that flit_core includes to solve the circular dependency on older Pythons
# (flit_core requires tomli, but flit_core is needed to build tomli).
# We don't use this, as tomllib is a part of standard library since Python 3.11.
rm -rf flit_core/vendor

%build
%{python3} -m flit_core.wheel

%install
%if %{with bootstrap}
%{python3} bootstrap_install.py --install-root %{buildroot} dist/flit_core-%{version}-py3-none-any.whl
%else
%py3_install_wheel flit_core-%{version}-py3-none-any.whl
%endif
# for consistency with %%pyproject_install:
rm %{buildroot}%{python3_sitelib}/flit_core-*.dist-info/RECORD

# don't ship tests in flit_core package
# if upstream decides to change the installation, it can be removed:
# https://github.com/takluyver/flit/issues/403
rm -r %{buildroot}%{python3_sitelib}/flit_core/tests/

%check
%py3_check_import flit_core flit_core.buildapi
%if %{with tests}
%pytest
%endif


%files -n python%{python3_pkgversion}-flit-core
%license LICENSE
%doc README.rst
%{python3_sitelib}/flit_core-*.dist-info/
%{python3_sitelib}/flit_core/


%changelog
* Tue Jan 23 2024 Miro Hrončok <mhroncok@redhat.com> - 3.9.0-3
- Rebuilt for timestamp .pyc invalidation mode

* Thu Nov 16 2023 Charalampos Stratakis <cstratak@redhat.com> - 3.9.0-2
- Disable bootstrap

* Tue Oct 10 2023 Tomáš Hrnčiar <thrnciar@redhat.com> - 3.9.0-1
- Initial package
- Fedora contributions by:
      Charalampos Stratakis <cstratak@redhat.com>
      Gwyn Ciesla <limb@fedoraproject.org>
      Igor Gnatenko <ignatenkobrain@fedoraproject.org>
      Karolina Surma <ksurma@redhat.com>
      Lumir Balhar <lbalhar@redhat.com>
      Maxwell G <maxwell@gtmx.me>
      Miro Hrončok <miro@hroncok.cz>
      Mukundan Ragavan <nonamedotc@gmail.com>
      Tomas Hrcka <thrcka@redhat.com>
      Tomáš Hrnčiar <thrnciar@redhat.com>
      Yaakov Selkowitz <yselkowi@redhat.com>

